import { Injectable } from '@angular/core';



interface AccountConstructorConfig {
	accountName: string;
	balance: number;
}

class Account {
	name: string;
	private balance: number;

	constructor(config: AccountConstructorConfig) {
		this.name = config.accountName;
		this.balance = config.balance;
	}

	/**
	 * Use this method instead of accessing this.balance directly.
	 * It simulates the fact that the bank needs some time to check the balance,
	 * it's not an instantaneous operation.
	 */
	private async getBalance(): Promise<number> {
		await new Promise(resolve => {
			setTimeout(resolve, 2000);
		});
		return this.balance;
	}

	/**
	 * Called at the begining of the withdrawal.
	 * Should return a boolean indicating whether the user is allowed
	 * to perform the withdrawal.
	 */
	async requestWithdrawal(amount: number): Promise<boolean> {
		console.log('Requesting withdrawal of ' + amount + '€ for account ' + this.name);

		const balance = await this.getBalance();

		return false;
	}

	/**
	 * Call this method to updated the account's balance
	 * once the withdrawal is complete.
	 */
	onWithdrawalComplete(amount: number) {
		this.balance -= amount;
	}

}





const INITIAL_ACCOUNTS_CONFIG: AccountConstructorConfig[] = [
	{ accountName: 'Ana LYSE', balance: 1000 },
	{ accountName: 'Tom CRUISE', balance: 6000000 },
	{ accountName: 'Johnny', balance: 2000 }
];

/**
 * AccountsService is a singleton class, which means that
 * it is instantiated only once, when it is first
 * imported by some component (e.g. atm.component.ts).
 */

@Injectable({
	providedIn: 'root'
})
export class AccountsService {

	private accounts: Account[];

	constructor() {
		this.accounts = INITIAL_ACCOUNTS_CONFIG.map(config => new Account(config));
	}

	/** Returns the account associated to accountName, or throws an error if not found. */
	getAccount(accountName: string): Account {
		const account = this.accounts && this.accounts.find(el => el.name === accountName);
		if(account)
			return account;
		else
			throw 'No account found for ' + accountName;			
	}
}
