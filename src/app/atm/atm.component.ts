import { Component, OnInit, Input } from '@angular/core';

import { AccountsService } from '../services/accounts.service';

@Component({
	selector: 'app-atm',
	templateUrl: './atm.component.html',
	styleUrls: ['./atm.component.scss']
})
export class AtmComponent implements OnInit {

	/** ID of this ATM */
	@Input() atmNumber: number;

	availableBankNotes: {
		/** Bank note value, e.g. 10€, 20€... */
		value: number;
		/** Number of available bank notes in this ATM, for the given value. */
		count: number;
	}[];

	// The following lines contain the values of the corresponding inputs in atm.component.html.
	// They are automatically updated by Angular every time the inputs are updated by the user.
	amountToWithdraw: number;
	accountToWithdrawFrom: string;

	withdrawing: boolean;

	constructor(
		private accountsService: AccountsService
		) { }

	ngOnInit(): void {
		this.availableBankNotes = [
			{ value: 10, count: Math.floor(30 * Math.random()) },
			{ value: 20, count: Math.floor(30 * Math.random()) },
			{ value: 50, count: Math.floor(30 * Math.random()) }
		];
	}

	async withdraw() {
		console.log('Withdrawing ' + this.amountToWithdraw + '€...');
		this.withdrawing = true;
		

		// Uncomment all the following lines for question 2

		// Account and balance checks
		// const account = this.accountsService.getAccount(this.accountToWithdrawFrom);
		// console.log('Account', account);

		// const canWithdraw = await account.requestWithdrawal(this.amountToWithdraw);
		// console.log('Can withdraw', canWithdraw);


		// if(canWithdraw) {
		// 	// Bank notes delivery
		// 	// Don't remove this, it simulates the fact that an ATM needs some time to deliver the bank notes
		// 	await new Promise(resolve => {
		// 		setTimeout(resolve, 3000);
		// 	});

		// 	// Update account's balance (once the money has been delivered)
		// 	account.onWithdrawalComplete(this.amountToWithdraw);
		// }



		this.withdrawing = false;
		this.amountToWithdraw = null;
	}

}
