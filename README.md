# JS test - EzyBank

## Installation

* Install NodeJS and npm.
* Install Angular CLI : `npm install -g @angular/cli`. This command may need to be run as admin (using `sudo` on Linux / MacOS or administrator mode on Windows).
* In this directory, run `npm install`.

## Development

* Start the local development server using `ng serve`.
* You should now be able to access the website at [http://localhost:4200](http://localhost:4200). The app will automatically reload if you change any of the source files.

### Node-sass possible error

If you get an error like `ERROR in Cannot find module 'node-sass'`, it might be that Node and node-sass versions don't match. 

More details [here](https://stackoverflow.com/questions/48147896/error-in-cannot-find-module-node-sass). As stated in [this answer](https://stackoverflow.com/a/57890029), you have to install the correct version of `node-sass` depending on your NodeJS version, by running something like `npm install node-sass@4.12.0`.
